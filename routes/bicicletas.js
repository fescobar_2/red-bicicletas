var express = require('express');
var router = express.Router();
var bicicletaController = require('../controllers/bicicleta');

router.get('/', bicicletaController.getAllBicis);
router.get('/create', bicicletaController.createBiciGet);
router.post('/create', bicicletaController.createBici);
router.get('/:id/update', bicicletaController.updateBiciGet);
router.post('/:id/update', bicicletaController.updateBici);
router.post('/:id/delete', bicicletaController.deleteBici);

module.exports = router;