var Bicicleta = require('../models/Bicicleta');

exports.getAllBicis = function(req,res) {
    res.render('bicicletas/index', { bicis: Bicicleta.allBicis })
}

exports.createBiciGet = function(req,res) {
    res.render('bicicletas/create');
}


exports.createBici = function(req,res) {
    var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.long];
    Bicicleta.add(bici);

    res.redirect('/bicicletas');
}

exports.updateBiciGet = function(req,res) {
    var bici = Bicicleta.findById(req.params.id);

    res.render('bicicletas/update',{bici});
}


exports.updateBici = function(req,res) {
    var bici = Bicicleta.findById(req.params.id);
    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.long];

    res.redirect('/bicicletas');
}

exports.deleteBici = function(req,res) {
    Bicicleta.removeById(req.body.id);
    res.redirect('/bicicletas');
}