var map = L.map('map', {
    center: [-25.299999, -57.630001],
    zoom: 13
});

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: ' <a href="https://www.openstreetmap.org/">OpenStreetMap</a> ',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiZmVzY29iYXItMiIsImEiOiJja2d2NXV0a3cxcTFmMnNyMzN3djA5Zm1xIn0._gqj3qqaP6O4aOelJ3P1dw'
}).addTo(map);

L.marker([-25.289999, -57.620001]).addTo(map);
L.marker([-25.279999, -57.610001]).addTo(map);
L.marker([-25.269999, -57.640001]).addTo(map);

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion, {title:bici.id}).addTo(map);
        });
    } 
})